﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoRotacionTorretaLaser : MonoBehaviour
{
    public float velocidadRotacion;
    
    public int signo = 1;

    private float tiempoRotacion = 2f;
    public float tiempoRotacionUsuario;

   // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       Timer(tiempoRotacionUsuario);
 
       Rotar(signo);  
    }

    public void Rotar(int signo)
    {    
        Quaternion rotacion = Quaternion.AngleAxis(velocidadRotacion, Vector3.up);
        
        transform.rotation = Quaternion.Euler(rotacion.x, rotacion.y * signo, rotacion.z) * transform.rotation;    
      
    }

    public void Timer(float tiempoDespues)
    {
        tiempoRotacion -= Time.deltaTime;
        if(tiempoRotacion < 0)
        {
            signo = signo * -1;
            tiempoRotacion = tiempoDespues;
        }
    }
}
