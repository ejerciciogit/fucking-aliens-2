﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TipoTorreta{STANDAR, LASER}

public class Torretas : Enemigo
{
    public TipoTorreta tipoTorreta;
    public float rangoTorreta;


    [Header("Rotacion de la torreta")]
    public Transform target;
    public Transform partToRotate;
    private float convertirVelocidad = 10f;


    [Header("Disparo de la torreta")]
    public GameObject balaTorreta;
    public Transform lugarDisparo;
    public float fuerzaBalaTorreta;
    public float fireRate = 1f;
    private float fireCountdown = 0f;
    // Start is called before the first frame update
    void Start()
    {
        if(tipoTorreta == TipoTorreta.STANDAR)
        {
            rangoTorreta = 25;
            vida = 80;
        }

        if (tipoTorreta == TipoTorreta.LASER)
        {
            rangoTorreta = 40;
            vida = 50;
        }

        InvokeRepeating("UpdateTarget", 0f, 0.5f);
    }

    void UpdateTarget()
    {
        GameObject[] objetivos = GameObject.FindGameObjectsWithTag("player");
        float nearestDistancia = Mathf.Infinity;
        GameObject nearestObjetivo = null;

        foreach (GameObject enemy in objetivos)
        {
            float distanciaObjetivo = Vector3.Distance(transform.position, enemy.transform.position);
            if(distanciaObjetivo < nearestDistancia)
            {
                nearestDistancia = distanciaObjetivo;
                nearestObjetivo = enemy;
            }
        }

        if(nearestObjetivo != null && nearestDistancia <= rangoTorreta)
        {
            target = nearestObjetivo.transform;
        }
        else
        {
            target = null;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(target == null)
        {
            return;
        }

        //Sistema de rotacion de la torreta:

        SetRotation();

        if(fireCountdown <= 0)
        {
            Shoot();
            fireCountdown = 1f / fireRate;
        }

        fireCountdown -= Time.deltaTime;

    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(transform.position, rangoTorreta);
    }

    public void SetRotation()
    {
        Vector3 dir = target.position - transform.position;

        //Rotacion con la informacion en quaterniones
        Quaternion lookRotation = Quaternion.LookRotation(dir);

        //Pasamos esa informacion de quaterniones a informacion de rotacion estandar de unity
        Vector3 rotacionReal = Quaternion.Lerp(partToRotate.rotation, lookRotation, Time.deltaTime * convertirVelocidad).eulerAngles;

        partToRotate.rotation = Quaternion.Euler(rotacionReal.x, rotacionReal.y, 0);

    }

    public void Shoot()
    {
        GameObject bulletTorreta = Instantiate(balaTorreta, lugarDisparo.position, lugarDisparo.rotation);
        Rigidbody rb = bulletTorreta.GetComponent<Rigidbody>();

        rb.AddForce(lugarDisparo.transform.forward * fuerzaBalaTorreta, ForceMode.Impulse);
    }
}

