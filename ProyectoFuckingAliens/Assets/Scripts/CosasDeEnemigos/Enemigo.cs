﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo : MonoBehaviour
{
    public float vida { get; set; }

    public float daño { get; set; }

    public void TakeDamage(float daño)
    {
        vida -= daño;
    }
}

