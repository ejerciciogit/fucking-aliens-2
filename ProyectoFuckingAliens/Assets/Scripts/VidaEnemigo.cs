﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TipoDeEnemigo { TORRETAESTANDAR, TORRETALASER, ENEMIGOQUECORRE }
public class VidaEnemigo : Enemigo
{
    public TipoDeEnemigo tipo;
    
    // Start is called before the first frame update
    void Start()
    {
        if (tipo == TipoDeEnemigo.TORRETAESTANDAR)
        {
            vida = 20;
        }

        if (tipo == TipoDeEnemigo.TORRETALASER)
        {
            vida = 35;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if(vida <= 0)
        {
            Destroy(gameObject);
        }
    }


}
