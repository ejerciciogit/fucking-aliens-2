﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaExplosivo : MonoBehaviour
{
    public GameObject explosionEfect;

    public float delay = 3f;

    public float radioExplosión = 20f;

    public float fuerzaExplosion = 700f;

    float countdown;

    public float dañoBala = 10f;
    // Start is called before the first frame update
    void Start()
    {
        countdown = delay;
        dañoBala = 10f;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer != 9)
        {
            Explode();
        }
        
    }

    /*private void OnCollisionEnter(Collider other)
    {

        if(other.gameObject.layer != 9)
        {
            Explode();
        }       
        
    }*/

    void Explode()
    {

        Instantiate(explosionEfect, transform.position, transform.rotation);

        Collider[] colliders = Physics.OverlapSphere(transform.position, radioExplosión);

        foreach (Collider nearbyObject in colliders)
        {
            Rigidbody rbExplosion = nearbyObject.GetComponent<Rigidbody>();

            if(rbExplosion != null)
            {
                rbExplosion.AddExplosionForce(fuerzaExplosion, transform.position, radioExplosión);
            }

            if(nearbyObject.gameObject.GetComponent<VidaEnemigo>() != null)
            {
                VidaEnemigo enemigoCosas = nearbyObject.gameObject.GetComponent<VidaEnemigo>();

                enemigoCosas.TakeDamage(dañoBala);

                //Destroy(gameObject);
            }
        }

        Destroy(gameObject);


    }

}
