﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparoRayoLaser : MonoBehaviour
{
    public Camera camera;

    public float alcance = 500f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Fire1"))
        {
            RaycastHit hit;
            Physics.Raycast(camera.transform.position, camera.transform.forward, out hit, alcance);

            Debug.DrawRay(camera.transform.position, camera.transform.forward);
        }
    }
}
