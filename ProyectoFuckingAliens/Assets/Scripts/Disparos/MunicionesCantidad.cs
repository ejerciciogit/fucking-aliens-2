﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MunicionesCantidad : MonoBehaviour
{
    public int municionStandar = 50;
    public int municionExplosiva = 20;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter(Collider other)
    {
        

        if(other.CompareTag("municionStandar") == true)
        {
            
            municionStandar += 20;
        }

        if (other.tag == "municionExplosiva")
        {
            municionExplosiva += 5;
        }
    }
}
