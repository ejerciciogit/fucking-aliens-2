﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaStandar : MonoBehaviour
{
    public float tiempoVida = 5f;
    public int daño = 10;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        tiempoVida -= Time.deltaTime;

        if(tiempoVida <= 0)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if(other.gameObject.GetComponent<VidaEnemigo>() != null)
        {
            VidaEnemigo enemigoCosas = other.gameObject.GetComponent<VidaEnemigo>();

            enemigoCosas.TakeDamage(daño);

            Destroy(gameObject);
        }
        
    }
}
