﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponManager : MonoBehaviour
{
    //[SerializeField]
    //private WeaponHandler[] weapons;

    //Forma de hacer todo esto con listas PREGUNTAR A SERGIO
    [SerializeField]
    public List<WeaponHandler> weaponsPrueba = new List<WeaponHandler>();

    private int current_Weapon_Index;

    public int armaQueTengo = 0;
    
    // Start is called before the first frame update
    void Start()
    {
        current_Weapon_Index = 0;
        weaponsPrueba[current_Weapon_Index].gameObject.SetActive(true);

        TurnOnSelectedWeapon(armaQueTengo);

    }

    // Update is called once per frame
    void Update()
    {
        //Hago el sistema de cambiar de arma
        if(Input.GetAxis("Mouse ScrollWheel") > 0 && armaQueTengo < weaponsPrueba.Count-1)
        {
            armaQueTengo++;
            TurnOnSelectedWeapon(armaQueTengo);
        }

        if (Input.GetAxis("Mouse ScrollWheel") < 0 && armaQueTengo > 0)
        {
            armaQueTengo--;
            TurnOnSelectedWeapon(armaQueTengo);
        }   

    }

    void TurnOnSelectedWeapon(int weaponIndex)
    {
        //Turn off Previous weapon
        weaponsPrueba[current_Weapon_Index].gameObject.SetActive(false);

        //Turn on Actual weapon
        weaponsPrueba[weaponIndex].gameObject.SetActive(true);

        //Guardo el arma actual
        current_Weapon_Index = weaponIndex;
    }

    public WeaponHandler GetCurrentSelectedWeapon()
    {
        return weaponsPrueba[current_Weapon_Index];
    }
}
