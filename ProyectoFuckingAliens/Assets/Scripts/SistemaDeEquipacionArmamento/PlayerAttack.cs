﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    private WeaponManager weapon_Manager;

    [Header ("Daño que será distinto para cada arma")]
    public float damage = 20;

    [Header("Variables del disparo Standar y del arma Explosiva")]
    
    //Variables para el arma standar y el arma explosiva///////////////////////////////////////////////////////////////////
    public GameObject balaStandar;
    public GameObject balaExplosiva;

    public float fuerzaBalaStandar = 5f;
    public float fuerzaBalaExplosiva = 5f;

    private float nexFire = 0.5f;
    private float myTime = 0.0f;
    public float velocidadDisparo = 0.1f;

    public Transform lugarDisparo;
    public Transform lugarDisparoExplosivo;

    public MunicionesCantidad municion;

    [Header("Variables del disparo Rayo")]
    //Variables para el arma rayo////////////////////////////////////////////////////////////////////////////////////////////

    public Camera camera;

    public float alcance = 500f;

    private void Awake()
    {
        weapon_Manager = GetComponent<WeaponManager>();
    }
    void Start()
    {
        
    }

    void Update()
    {
        WeaponShoot();
    }

    void WeaponShoot()
    {
        //Si tenemos el arma standar
        if(weapon_Manager.GetCurrentSelectedWeapon().fireType == WeaponFireType.DISPARONOMRAL)
        {
            myTime += Time.deltaTime;

            if (Input.GetButton("Fire1") && myTime > nexFire && municion.municionStandar > 0)
            {
                nexFire = myTime + velocidadDisparo;

                GameObject bullet = Instantiate(balaStandar, lugarDisparo.position, transform.rotation);
                Rigidbody rbBala = bullet.GetComponent<Rigidbody>();

                rbBala.AddForce(lugarDisparo.transform.forward * fuerzaBalaStandar, ForceMode.Impulse);

                nexFire = nexFire - myTime;
                myTime = 0.0f;
                municion.municionStandar--;
            }
        }

        if(weapon_Manager.GetCurrentSelectedWeapon().fireType == WeaponFireType.EXPLOSIVO)
        {
            myTime += Time.deltaTime;

            if (Input.GetButton("Fire1") && myTime > nexFire && municion.municionExplosiva > 0)
            {
                nexFire = myTime + velocidadDisparo;

                GameObject bullet = Instantiate(balaExplosiva, lugarDisparoExplosivo.position, transform.rotation);
                Rigidbody rbBala = bullet.GetComponent<Rigidbody>();

                rbBala.AddForce(lugarDisparoExplosivo.transform.forward * fuerzaBalaExplosiva, ForceMode.Impulse);

                nexFire = nexFire - myTime;
                myTime = 0.0f;
                municion.municionExplosiva--;
            }
        }

        if(weapon_Manager.GetCurrentSelectedWeapon().fireType == WeaponFireType.RAYO)
        {
            if (Input.GetButton("Fire1"))
            {
                RaycastHit hit;
                Physics.Raycast(camera.transform.position, camera.transform.forward, out hit, alcance);
                Debug.DrawRay(camera.transform.position, camera.transform.forward);
            }
        }
    }
}
