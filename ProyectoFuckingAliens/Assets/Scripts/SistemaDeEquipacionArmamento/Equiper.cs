﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Equiper : MonoBehaviour
{

    public WeaponHandler arma;
    public void OnTriggerEnter(Collider other)
    {
        if(other.tag == "player")
        {
            //Debug.Log("He añadido un arma");
            if (other.GetComponentInChildren<WeaponManager>() != null)
            {
                if(other.GetComponentInChildren<WeaponManager>().weaponsPrueba.Contains(arma) == false)
                {
                    Debug.Log("He añadido un arma");
                    other.GetComponentInChildren<WeaponManager>().weaponsPrueba.Add(arma);
                }
                
            }
                     
        }
    }
}
