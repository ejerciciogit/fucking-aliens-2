﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Weapon 
{
    public int armaID;
    public GameObject arma;

    public float daño;
    public float cadencia;

    public float vidaBala;
}
