﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BalasCanvas : MonoBehaviour
{
    public Text Balas;
    public MunicionesCantidad municiones;

    private WeaponManager weapon_Manager;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (weapon_Manager.GetCurrentSelectedWeapon().fireType == WeaponFireType.DISPARONOMRAL)
        {
            Balas.text = municiones.municionStandar.ToString();
        }

        if (weapon_Manager.GetCurrentSelectedWeapon().fireType == WeaponFireType.EXPLOSIVO)
        {
            Balas.text = municiones.municionExplosiva.ToString();
        }

    }
    
   
}
