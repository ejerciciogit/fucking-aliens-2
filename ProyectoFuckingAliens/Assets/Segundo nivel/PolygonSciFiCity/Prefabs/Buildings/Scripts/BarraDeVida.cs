﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraDeVida : MonoBehaviour
{
    public Slider slider;
    public ProtaManager vidaProta;
    // Start is called before the first frame update
    void Start()
    {
        vidaProta = GetComponent<ProtaManager>();
    }

    // Update is called once per frame
    void Update()
    {
        SetHealth(vidaProta.vida);
    }

    public void SetHealth(int vida)
    {
        slider.value = vida;
    }
}
